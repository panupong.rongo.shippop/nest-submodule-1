import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { LoggerModule } from 'nestjs-pino'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { CatsModule } from './cats/cats.module';
import configuration from './config/configuration'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], // ตั้งค่าตัวแปร env file path ที่จะอ่าน
      load: [configuration],
      isGlobal: true,
    }),
    LoggerModule.forRoot({
      // pinoHttp: {
      //   transport: {
      //     target: 'pino-pretty',
      //     options: {
      //       colorize: true,
      //       messageFormat:
      //         '{levelLabel} - {pid} - url:{request.url} - {context}',
      //     },
      //   },
      // },
    }),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
